﻿Shader "Custom/UnlitSolidColor" {
Properties {
	_Color ("Color", Color) = (1,1,1,1)
}
SubShader {
Pass {

Tags { 
	"RenderType"="Opaque"
	"Queue"="Overlay+20" 
}
ZTest Always
Lighting Off
Blend One Zero

CGPROGRAM

#pragma vertex vert
#pragma fragment frag

fixed4 _Color;

// vertex input: position, color
struct appdata {
	float4 vertex : POSITION;
};

struct v2f {
	float4 pos : SV_POSITION;
};
		
v2f vert (appdata v) {
	v2f o;
	o.pos = mul( UNITY_MATRIX_MVP, v.vertex );
	return o;
}
		
fixed4 frag (v2f i) : COLOR0 { return _Color; }
ENDCG
	}
}
}