﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ContinueButton : MonoBehaviour {

	public float buttonWidth, buttonHeight;
	public MoveByTouch moveByTouch;
	public MovesLeft movesLeft;
	public FogOfWarRenderer fogOfWar;

	protected void OnGUI() {
		Rect buttonRect = new Rect(Screen.width / 2 - buttonWidth / 2, 10, buttonWidth, buttonHeight);
		if (GUI.Button(buttonRect, "Continue!")) {
			moveByTouch.MoveBackCamera();
			gameObject.SetActive(false);
			movesLeft.ResetMoves();
			fogOfWar.Reset();
		}
	}

}
