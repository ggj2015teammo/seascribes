﻿using UnityEngine;
using System.Collections;


public class MouseInputPosition : MonoBehaviour
{
	Vector3 mousePos, mousePosPrev;
	public float LineHalfWidth;

	void Awake()
	{
		MapGenerator.Instance.GetMap();
	}

	void Update()
	{
		if (!Input.GetMouseButton(0))
		{
			mousePosPrev = mousePointToWorld();
			return;
		}

		mousePos = mousePointToWorld();

		MapGenerator map = MapGenerator.Instance;

		map.BeginDraw();
		map.DrawLine(mousePosPrev, mousePos, LineHalfWidth);
		map.EndDraw();


		Debug.Log("Pos: " + mousePos + " PosPrev: " + mousePosPrev);
		mousePosPrev = mousePos;
	}

	private static Vector2 mousePointToWorld()
	{
		var c = Camera.main;
		Ray r = c.ScreenPointToRay(Input.mousePosition);
		Plane p = new Plane(new Vector3(0f, 0f, 0f), new Vector3(0f, 1f, 0f), new Vector3(1f, 0f, 0f));

		float distance;
		if (!p.Raycast(r, out distance)) return Vector2.zero;

		return r.GetPoint(distance);
		//Vector3 clickPoint = r.GetPoint(distance);
	}
}

