﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MapGenerator : MonoBehaviour
{
	private static MapGenerator _mapGenInst;
	public static MapGenerator Instance { get { return _mapGenInst; } }

	private struct Line
	{
		public readonly Vector2 A;
		public readonly Vector2 B;
		public readonly float HalfWidth;

		public Line(Vector2 a, Vector2 b, float halfWidth)
		{
			A = a;
			B = b;
			HalfWidth = halfWidth;
		}
	}

	private List<Line> _lines = new List<Line>();

	public Material LineMaterial;
	public int MapWidth, MapHeight;

	void Awake()
	{
		_mapGenInst = this;
		BeginDraw();
		EndDraw ();
	}

	void Destroy()
	{
		_mapGenInst = null;
	}

	private RenderTexture _renderTex;
	public RenderTexture GetMap()
	{
		if (_renderTex != null) return _renderTex;
		_renderTex = new RenderTexture(MapWidth, MapHeight, 0);
		return _renderTex;
	}

	public void TestForRecreate()
	{
		if (_renderTex.IsCreated()) return;
		_renderTex.Create();
		BeginDraw();
		foreach (Line l in _lines)
			DrawLine(l.A, l.B, l.HalfWidth);
		EndDraw();
	}

	public void BeginDraw()
	{
		bool recreating = !GetMap().IsCreated();
		if (recreating)
		{
			//Debug.Log("Recreating");
			_renderTex.Create();
		}

		RenderTexture.active = _renderTex;

		if (recreating) GL.Clear(true, true, Color.black);

		GL.PushMatrix();
		GL.LoadPixelMatrix(0f, _renderTex.width, 0f, _renderTex.height);

		LineMaterial.SetPass(0);


		GL.Color(Color.white);
		GL.Begin(GL.QUADS);


		if (recreating)
		{
			foreach (Line l in _lines)
				DrawLine(l.A, l.B, l.HalfWidth);
		}
	}

	public void DrawLine(Vector2 a, Vector2 b, float halfWidth)
	{
		float swap = a.x;
		a.x = a.y;
		a.y = swap;
		swap = b.x;
		b.x = b.y;
		b.y = swap;

		//Debug.Log("Draw len: " + (b - a).magnitude);
		Vector2 dir = (b - a).normalized;
		swap = dir.x;
		dir.x = -dir.y;
		dir.y = swap;

		dir *= halfWidth;

		GL.TexCoord(new Vector2(1f, 0f));
		GL.Vertex(a - dir);
		GL.TexCoord(Vector2.zero);
		GL.Vertex(a + dir);
		GL.TexCoord(new Vector2(0f, 1f));
		GL.Vertex(b + dir);
		GL.TexCoord(new Vector2(1f, 1f));
		GL.Vertex(b - dir);

		_lines.Add(new Line(a, b, halfWidth));
	}

	public void EndDraw()
	{
		GL.End();
		GL.PopMatrix();

		RenderTexture.active = null;
	}
}
