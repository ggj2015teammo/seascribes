﻿using UnityEngine;
using System.Collections;

public class MapDrawer : MonoBehaviour
{
	Vector2? previousPoint = null;
	Camera _mainCam;

	public float Height;

	void Awake()
	{
		_mainCam = Camera.main;


		MapGenerator mapGen = MapGenerator.Instance;
	}

	void Update()
	{
		if (!Input.GetMouseButton(0))
		{
			previousPoint = null;
			return;
		}
		
		Vector2 uv = getMapUVFromScreenPoint(_mainCam, Input.mousePosition, Height);

		if (previousPoint.HasValue && (previousPoint.Value - uv).sqrMagnitude >= 100f)
		{
			MapGenerator mapGen = MapGenerator.Instance;
			mapGen.BeginDraw();
			mapGen.DrawLine(previousPoint.Value, uv, 2f);
			mapGen.EndDraw();
			previousPoint = uv;
		}
		else if (!previousPoint.HasValue)
			previousPoint = uv;
	}

	private Vector2 getMapUVFromScreenPoint(Camera camera, Vector3 screenPoint, float height)
	{
		// Projection plane
		Plane xz = new Plane(new Vector3(0f, 1f, 0f), new Vector3(0f, height, 0f));

		// Raycast into screen
		Ray ray = camera.ScreenPointToRay(screenPoint);

		float distance;
		if (!xz.Raycast(ray, out distance))
			return Vector2.zero;
		Vector3 point = ray.GetPoint(distance);
		return new Vector2(point.x, point.z);
	}
}
