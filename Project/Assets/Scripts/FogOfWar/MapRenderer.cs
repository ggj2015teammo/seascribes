﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(MeshRenderer), typeof(MeshFilter))]
public class MapRenderer : MonoBehaviour
{
	/// <summary>
	/// Generates plane the size of the RenderTexture
	/// </summary>
	private void generateMesh()
	{
		RenderTexture map = MapGenerator.Instance.GetMap();
		GetComponent<MeshFilter>().mesh =
			MeshGenerator.CreateQuad(Vector2.zero, new Vector2(map.width * 0.5f, map.height * 0.5f));
	}

	private void setShaderTexture()
	{
		GetComponent<MeshRenderer>().material.SetTexture("_MainTex", MapGenerator.Instance.GetMap());
	}


	void Awake()
	{
		generateMesh();
		setShaderTexture();
	}
}
