﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(MeshRenderer), typeof(MeshFilter))]
public class FogOfWarRenderer : MonoBehaviour
{
	public int Width, Height;
	public Transform FogClearingObject;
	public Texture FogClearTexture;
	public Material FogClearMaterial;
	public Vector2 FogClearSize;

	private RenderTexture _fogTex;
	private Vector3 _prevPos;
	private float _warmUp = 0.5f;
	private float _fillAcc = 0f;

	void Awake()
	{
		_fogTex = new RenderTexture(Width, Height, 0);

		if (!_fogTex.IsCreated())
		{
			_fogTex.Create();

			Reset();
		}

		GetComponent<MeshRenderer>().material.SetTexture("_MainTex", _fogTex);

		_prevPos = FogClearingObject.position;
	}

	void Update()
	{
		Vector3 position = FogClearingObject.position;

		if (_warmUp > 0f) _warmUp -= Time.deltaTime;
		else
		{
			_fillAcc = Mathf.Max(0f, Mathf.Min(0.7f, _fillAcc + Time.deltaTime - (position - _prevPos).magnitude * 0.25f));
			if (_fillAcc > 0.5f) return;
		}

		RenderTexture oldActive = RenderTexture.active;
		RenderTexture.active = _fogTex;
		
		GL.PushMatrix();
		GL.LoadPixelMatrix(0f, Width, 0f, Height);
		Graphics.DrawTexture(
			new Rect(
				position.x - FogClearSize.x * 0.5f,
				position.z - FogClearSize.y * 0.5f,
				FogClearSize.x, FogClearSize.y),
			FogClearTexture, FogClearMaterial);

		GL.PopMatrix();

		RenderTexture.active = oldActive;
	}

	public void Reset() 
	{
		_warmUp = 1f;
		RenderTexture oldActive = RenderTexture.active;
		RenderTexture.active = _fogTex;
		GL.Clear(true, true, Color.black);
		RenderTexture.active = oldActive;
	}
}
