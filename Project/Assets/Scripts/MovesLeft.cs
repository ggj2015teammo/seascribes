﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MovesLeft : MonoBehaviour {


	public float textureDisplaySize = 40;
	public float textureDisplayOffset = 10;

	public int nrOfMoves;
	public int movesLeft;
	public Texture image;

	protected void Start() {
		ResetMoves();
	}

	public void ResetMoves() {
		movesLeft = nrOfMoves;
	}

	public bool CanMove() {
		return movesLeft > 0;
	}

	public void SpendMove() {
		movesLeft -= 1;
	}

	protected void OnGUI() {
		float width = nrOfMoves * (textureDisplaySize + textureDisplayOffset);

		//Draw box
		Rect rectangle = new Rect(Screen.width / 2 - width / 2 - textureDisplayOffset, 0, width + textureDisplayOffset * 2, textureDisplaySize + textureDisplayOffset * 2);
		RectDraw.DrawRect(rectangle, new Color(0,0,0,0.1f));
		RectDraw.DrawRect(rectangle, new Color(0,0,0,0.6f), 1);
		
		//Draw moves
		for (int i = 0; i < movesLeft; i++) {
			Rect rect = new Rect(Screen.width / 2 - width / 2 + (textureDisplaySize + textureDisplayOffset) * i, textureDisplayOffset, textureDisplaySize, textureDisplaySize);
			GUI.DrawTexture(rect, image);
		}
	}
}
