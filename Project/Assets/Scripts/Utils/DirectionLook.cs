using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class DirectionLook : MonoBehaviour 
{
	private Vector3 previousPosition;
	
	void Update () 
	{
		if ((previousPosition - this.transform.position).sqrMagnitude > 0.2) {
			Vector3 direction = (this.transform.position - previousPosition).normalized;
			this.transform.rotation = Quaternion.LookRotation(direction, Vector3.up);
			previousPosition = this.transform.position;
		}
	}
}
