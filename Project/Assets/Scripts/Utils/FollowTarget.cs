using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class FollowTarget : MonoBehaviour 
{
	public GameObject target;
	public Vector3 offset;
	
	protected void Update () 
	{
		if (target != null)
			this.transform.position = target.transform.position + offset;
	}
}
