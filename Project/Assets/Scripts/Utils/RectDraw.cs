using UnityEngine;
using System.Collections;

public class RectDraw 
{
	private static Texture2D _rectTex;
	private static Texture2D rectTex
	{
		get
		{
			if (_rectTex == null)
			{
				_rectTex = new Texture2D(1, 1);
				_rectTex.SetPixels(new [] { Color.white });
				_rectTex.Apply();
			}
			return _rectTex;
		}
	}
	
	//Draw Rect
	public static void DrawRect(Rect rect, Color color)
	{
		DrawRectImpl(rect, color);
	}
	
	//Draw Rect (outline)
	public static void DrawRect(Rect rect, Color color, float outline)
	{
		DrawRectImpl(new Rect(rect.x, 				rect.y, 				rect.width, outline		), color);
		DrawRectImpl(new Rect(rect.x, 				rect.y, 				outline,	rect.height	), color);
		DrawRectImpl(new Rect(rect.x, 				rect.yMax - outline, 	rect.width, outline		), color);
		DrawRectImpl(new Rect(rect.xMax - outline,	rect.y, 				outline, 	rect.height	), color);
	}
	
	//Implementation of drawing a rect
	private static void DrawRectImpl(Rect r, Color color)
	{
		Color oldColor = GUI.color;
		Color oldContentColor = GUI.contentColor;
		GUI.color = color;
		GUI.contentColor = color;
		
		GUI.DrawTexture(r, rectTex);
		
		GUI.color = oldColor;
		GUI.contentColor = oldContentColor;
	}
}