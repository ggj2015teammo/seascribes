using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class LookAt : MonoBehaviour 
{
	public 	GameObject 	target;
	
	protected void Update() 
	{
		if (target != null)
			this.transform.LookAt(target.transform, Vector3.up);
	}
}
