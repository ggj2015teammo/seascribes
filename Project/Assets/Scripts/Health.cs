﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Health : MonoBehaviour {
	public float textureDisplaySize = 40;
	public float textureDisplayOffset = 10;
	
	public int startLives;
	public int livesLeft;
	public Texture image;
	
	protected void Start() {
		ResetLives();
	}
	
	public void ResetLives(int extra = 0) {
		livesLeft = startLives + extra;
	}
	
	public bool IsAlive() {
		return livesLeft > 0;
	}
	
	public void Hit() {
		livesLeft -= 1;
	}

	public void ReplenishLive() {
		livesLeft = Mathf.Max(livesLeft + 1, startLives);
	}
	
	protected void OnGUI() {

		float width = startLives * (textureDisplaySize + textureDisplayOffset);

		//Draw box
		Rect rectangle = new Rect(Screen.width / 2 - width / 2 - textureDisplayOffset, Screen.height - textureDisplayOffset * 2 - textureDisplaySize, width + textureDisplayOffset * 2, textureDisplaySize + textureDisplayOffset * 2);
		RectDraw.DrawRect(rectangle, new Color(0,0,0,0.1f));
		RectDraw.DrawRect(rectangle, new Color(0,0,0,0.6f), 1);

		//Draw lives
		for (int i = 0; i < livesLeft; i++) {
			Rect rect = new Rect(Screen.width / 2 - width / 2 + (textureDisplaySize + textureDisplayOffset) * i, Screen.height - textureDisplaySize - 10, textureDisplaySize, textureDisplaySize);
			GUI.DrawTexture(rect, image);
		}
	}
}
