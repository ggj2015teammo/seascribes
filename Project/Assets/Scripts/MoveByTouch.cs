﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MoveByTouch : MonoBehaviour {

	private Transform _trans;
	private Transform trans {
		get {
			if (_trans == null)
				_trans = transform;
			return _trans;
		}
	}

	private Rigidbody _rb;
	private Rigidbody body {
		get {
			if (_rb == null)
				_rb = rigidbody;
			return _rb;
		}
	}

	private MovesLeft _ml;
	private MovesLeft movesLeft {
		get {
			if (_ml == null)
				_ml = GetComponent<MovesLeft>();
			return _ml;
		}
	}

	//Info
	private int currentFingerId = -1;
	private bool atTarget = true;
	private Vector3 target = Vector3.zero;
	private bool released = true;
	public MapDrawer mapDrawer;
	public GameObject passOnUI;

	//Options
	public float speed;
	public float minimalDistance;
	public float reachTargetDistance;
	public Transform targetVisualizer;
	public AudioSource shipMoveSound;

	protected void Awake() {
		Screen.orientation = ScreenOrientation.LandscapeLeft;
	}

	protected void Update() {
		if ((!movement))
			return;

		//Check whether at the target and a new target gets specified
		Vector3 touchLocation;
		if (movesLeft.CanMove() && atTarget && GetTouchLocation(out touchLocation)) {
			touchLocation.y = 0;

			Vector3 diff = (touchLocation - trans.position);
			//Make sure you don't click to close to the boat
			if (diff.magnitude > minimalDistance) {
				//Specify new target
				atTarget = false;
				target = touchLocation;
				targetVisualizer.position = touchLocation;
				movesLeft.SpendMove();
			}
		}

		//When not yet at the target move towards it
		if (!atTarget) {
			Vector3 diff = (target - trans.position);
			float magnitude = diff.magnitude;

			if (magnitude > (1 / body.rigidbody.drag) * body.velocity.magnitude) {
				body.AddForce(diff / magnitude * speed * Time.deltaTime);
				if (!shipMoveSound.isPlaying)
					shipMoveSound.Play ();
			} 

			if (magnitude < reachTargetDistance) {
				if (shipMoveSound.isPlaying)
					shipMoveSound.Stop();
				ClearTarget();
			}
		}
	}

	public void ClearTarget() {
		this.atTarget = true;
		targetVisualizer.position += Vector3.down * 1000;
		if (!movesLeft.CanMove()) {
			mapDrawer.gameObject.SetActive(true);
			DisableMovement();
			StartCoroutine(panOutCamera(new Vector3(1000f, 1411f, 1000f)));//911f+69f
		}
	}

	Vector3 standardPosition;
	Quaternion standardRotation;

	IEnumerator panOutCamera(Vector3 panTo)
	{
		Transform camera = Camera.main.transform;
		standardPosition = camera.localPosition;
		standardRotation = camera.rotation;

		yield return new WaitForSeconds(0.5f);

		camera.GetComponent<LookAt>().enabled = false;
		Vector3 deltaPos = panTo - camera.position;
		Vector3 startPos = camera.localPosition;
		Vector3 endPos = startPos + deltaPos;
		Quaternion startRot = camera.rotation;
		Quaternion endRot = Quaternion.LookRotation(Vector3.down);
		
		Timer t = new Timer(1f);
		
		while (t) {
			float p = Easing.CubicInOut (t.progress);
			camera.localPosition = Vector3.Lerp(startPos, endPos, p);
			camera.rotation = Quaternion.Slerp(startRot, endRot, p);
			yield return null;
		}
		
		camera.localPosition = endPos;
		camera.rotation = endRot;
		// Done
	}
	
	private bool movement = true;
	public void DisableMovement() {
		movement = false;
		released = false;
	}
	public void EnableMovement() {
		movement = true;
		released = false;
	}

	public Coroutine MoveBackCamera() {
		return StartCoroutine(panInCamera());
	}

	private IEnumerator panInCamera() {
		yield return new WaitForSeconds(0.5f);

		passOnUI.SetActive(true);
		
		Transform camera = Camera.main.transform;
		camera.GetComponent<LookAt>().enabled = false;
		Vector3 startPos = camera.localPosition;
		Vector3 endPos = standardPosition;
		Quaternion startRot = camera.rotation;
		Quaternion endRot = standardRotation;
		
		Timer t = new Timer(1f);
		
		while (t) {
			float p = Easing.CubicInOut (t.progress);
			camera.localPosition = Vector3.Lerp(startPos, endPos, p);
			camera.rotation = Quaternion.Slerp(startRot, endRot, p);
			yield return null;
		}
		
		camera.localPosition = endPos;
		camera.rotation = endRot;

	}

	private bool GetTouchLocation(out Vector3 touchLocation) {
		touchLocation = Vector3.zero;

		//Check if currentFingerId was released
		//This is done before processing and finding new touches to make sure new touches can be found in the same frame as releasing
		foreach (Touch touch in Input.touches) {
			if ((touch.fingerId == currentFingerId) && (touch.phase == TouchPhase.Ended || touch.phase == TouchPhase.Canceled)) {
				released = true;
				currentFingerId = -1;
			}
		}

		if (released) {
			//Check if new touch found and process it
			foreach (Touch touch in Input.touches) {
				//Find new touch
				bool validTouch = (touch.phase == TouchPhase.Began || touch.phase == TouchPhase.Moved || touch.phase == TouchPhase.Stationary);
				if ((currentFingerId < 0) && validTouch) {
					currentFingerId = touch.fingerId;
				}

				//Proces current touch
				if (currentFingerId == touch.fingerId) {
					if (ScreenSeaRay(touch.position, out touchLocation)) {
						return true;
					}
				}
			}
		}
		
		// [DEBUG] Test for mouse input
		if (released && Input.GetMouseButton(0)) {
			if (ScreenSeaRay(Input.mousePosition, out touchLocation)) {
				return true;
			}
		}
		if (!Input.GetMouseButton(0)) {
			released = true;
		}

		//No touch found
		return false;
	}

	//Shoot ray from screen to the sea and return whether a sea hit was found
	private bool ScreenSeaRay(Vector3 screenPosition, out Vector3 collisionPoint)
	{
		screenPosition.z = 5f;
		Ray ray = Camera.main.ScreenPointToRay(screenPosition);

		int layerMask = 1 << LayerMask.NameToLayer("Sea");// | (1 << LayerMask.NameToLayer("Terrain"));
		RaycastHit hit;
		if (Physics.Raycast(ray, out hit, float.PositiveInfinity, layerMask)) {
			//Check whether we hit the sea or an island
			if (hit.transform.gameObject.layer == LayerMask.NameToLayer("Sea")) {
				Debug.DrawLine(hit.point, hit.point + Vector3.up, Color.green, 1f);

				collisionPoint = hit.point;
				return true;
			}
		}

		//No hit found
		collisionPoint = Vector3.zero;
		return false;
	}
}
