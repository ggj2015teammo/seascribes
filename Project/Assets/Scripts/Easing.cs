﻿using UnityEngine;

public static class Easing
{
	public enum EaseMethod
	{
		Linear,
		SinInOut,
		SinIn,
		SinOut,
		CubicInOut,
		CubicIn,
		CubicOut
	}

	public static float Ease(EaseMethod method, float t)
	{
		switch (method)
		{
			case EaseMethod.Linear:
				return t;
			case EaseMethod.SinInOut:
				return SinInOut(t);
			case EaseMethod.SinIn:
				return SinIn(t);
			case EaseMethod.SinOut:
				return SinOut(t);
			case EaseMethod.CubicInOut:
				return CubicInOut(t);
			case EaseMethod.CubicIn:
				return CubicIn(t);
			case EaseMethod.CubicOut:
				return CubicOut(t);
			default:
				throw new System.ArgumentException("Easing method '" + method + "' is not valid.", "method");
		}
	}

	private const float PI = Mathf.PI;
	private const float PIOVER2 = PI / 2f;
	private const float PIOVER4 = PI / 4f;
	private const float PIOVER6 = PI / 6f;
	private const float PI2 = PI * 2f;

	public static float SinInOut(float t)
	{
		return Mathf.Sin(t * PI - PIOVER2) * 0.5f + 0.5f;
	}

	public static float SinIn(float t)
	{
		return 1 - Mathf.Cos(t * PIOVER2);
	}

	public static float SinOut(float t)
	{
		return Mathf.Sin(t * PIOVER2);
	}

	public static float CubicIn(float t)
	{
		return t * t * t;
	}

	public static float CubicOut(float t)
	{
		return CubicIn(t - 1) + 1;
	}

	public static float CubicInOut(float t)
	{
		return t <= 0.5
			? (CubicIn(2f * t) * 0.5f)
			: (CubicOut(2f * t - 1f) * 0.5f + 0.5f);
	}

	public static float Wave(float t)
	{
		float mod = t % 2f;
		return mod > 1f
			? 2f - mod
			: mod;
	}

	public static float Clamp(float t)
	{
		return Mathf.Clamp01(t);
	}
}
