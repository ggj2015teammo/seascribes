﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Boat : MonoBehaviour {

	private Transform _trans;
	private Transform trans {
		get {
			if (_trans == null)
				_trans = transform;
			return _trans;
		}
	}

	private MoveByTouch _mbt;
	private MoveByTouch moveByTouch {
		get {
			if (_mbt == null)
				_mbt = GetComponent<MoveByTouch>();
			return _mbt;
		}
	}

	private Health _bh;
	private Health boatHealth {
		get {
			if (_bh == null)
				_bh = GetComponent<Health>();
			return _bh;
		}
	}

	public WinPopup winPopup;
	public Transform kraken;
	private Vector3 lastHitLocation = Vector3.zero;
	public AudioSource shipHitSound;

	public Vector3 startPosition;
	public MovesLeft movesLeft;

	protected void Start() {
		this.startPosition = trans.position;
	}

	protected void OnCollisionEnter(Collision collision) {
		if (CheckLastHitLocation()) {
			ContactPoint c = collision.contacts[0];
			if (c.otherCollider.gameObject.layer == LayerMask.NameToLayer("Terrain")) {
				moveByTouch.ClearTarget();
				boatHealth.Hit();
				shipHitSound.Play();

				if (!boatHealth.IsAlive()){
					ResetBoatToStart();
				}
			}
		}
	} 

	protected void Update() {
		Vector3 krakenPosition = kraken.position;
		krakenPosition.y = 0;

		float distanceToKraken = (krakenPosition - trans.position).magnitude;

		if (distanceToKraken < 80) {
			winPopup.Show();
		}
	}

	private void ResetBoatToStart() {
		//Reset boat to start
		trans.position = this.startPosition;
		//Reset moves
		movesLeft.ResetMoves();
		//Reset lives to -2 of normal start
		boatHealth.ResetLives();
	}

	//Check whether we moved since the last hit
	private bool CheckLastHitLocation() {
		Vector3 currentHitLocation = trans.position;
		
		if ((currentHitLocation - lastHitLocation).magnitude > moveByTouch.minimalDistance) {
			lastHitLocation = currentHitLocation;
			return true;
		}
		return false;
	}

}
