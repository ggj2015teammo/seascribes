﻿using UnityEngine;
using System.Collections;

public class PassOnUI : MonoBehaviour
{
	private const string GeneralInstruction =
@"Hold your thumb on one side of the device,
while the new player holds it on the other side.";

	private const string StartingIn = "Starting in... ";

	private bool _countingDown;

	public UnityEngine.UI.Text Instructions;
	public MoveByTouch moveByTouch;

	void Awake()
	{
		Instructions.text = GeneralInstruction;
	}

	void Update()
	{
		bool touchLeft = false, touchRight = false;
		for (int i = 0; i < Input.touchCount; i++)
		{
			var touch = Input.GetTouch(i);
			if (touch.position.x * 2f < Screen.width)
				touchLeft = true;
			else touchRight = true;
		}

		touchLeft = touchLeft || Input.GetKey(KeyCode.F1);
		touchRight = touchRight || Input.GetKey(KeyCode.F2);

		if (touchLeft && touchRight)
		{
			if (!_countingDown)
			{
				StartCoroutine(countDown());
				_countingDown = true;
			}
		}
		else if (_countingDown)
		{
			StopAllCoroutines();
			_countingDown = false;
			Instructions.text = GeneralInstruction;
		}
	}

	private IEnumerator countDown()
	{
		var wait1Sec = new WaitForSeconds(1f);

		for (int counter = 3; counter > 0; counter--)
		{
			Instructions.text = StartingIn + counter;
			yield return wait1Sec;
		}

		Instructions.text = string.Empty;
		_countingDown = false;

		// Go do next thing
		moveByTouch.EnableMovement();
		gameObject.SetActive(false);
	}

}
