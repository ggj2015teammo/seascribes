﻿using UnityEngine;
using System.Collections;

public static class MeshGenerator
{
	public static Mesh CreateQuad(Vector2 center, Vector2 halfSize)
	{
		Mesh m = new Mesh();

		m.vertices = new Vector3[]
		{
			center - halfSize,
			center + new Vector2(halfSize.x, -halfSize.y),
			center + new Vector2(-halfSize.x, halfSize.y),
			center + halfSize
		};
		m.uv = new Vector2[]
		{
			new Vector2(0f, 0f),
			new Vector2(0f, 1f),
			new Vector2(1f, 0f),
			new Vector2(1f, 1f)
		};
		m.normals = new Vector3[]
		{
			Vector3.back,
			Vector3.back,
			Vector3.back,
			Vector3.back,
		};
		m.triangles = new int[] { 0, 2, 1, 1, 2, 3 };

		return m;
	}
}
