﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class KrakenAnimation : MonoBehaviour {

	public float offset, speed;

	protected void Start() {
		animation["Default Take"].normalizedTime = Mathf.Repeat(offset, 1f);
		animation["Default Take"].normalizedSpeed = speed;
	}
}
