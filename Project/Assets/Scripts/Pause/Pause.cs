﻿using UnityEngine;
using System.Collections;

public class Pause : MonoBehaviour
{

	public Canvas MenuOverlay;
	private Coroutine coroutine; 

	float timeWaiting = 0.0f;

	public void MenuOverlayStart()
	{
		MenuOverlay.gameObject.SetActive(true);
		coroutine = StartCoroutine(timer());
		Debug.Log("Overlay Started.");
	}

	public void MenuOverlayStop()
	{
		MenuOverlay.gameObject.SetActive(false);
		Debug.Log("Overlay Stopped.");
	}

	public void GamePause()
	{
		Time.timeScale = 0.0f;
		Debug.Log("Game Paused.");
	}

	public void GameResume()
	{
		Time.timeScale = 1.0f;
		Debug.Log("Game Resumed.");
	}

	IEnumerator timer()
	{

		for (int i = 0; i < 5; i++) {
			timeWaiting++;
			Debug.Log("Time: " + timeWaiting);
			WaitForSeconds t = new WaitForSeconds(1f);
			yield return t;
		}


	}


	void Start() {
		MenuOverlayStart();
		//StopCoroutine(coroutine);
	}
}









/*
	//public float MenuFactorWidth, MenuFactorHeight;

	//public GUISkin myskin;
	//private Rect windowRect;	
	//private bool MenuOverlayEnabled = false;
	// Use this for initialization
	void Start () {
		windowRect = new Rect ( (Screen.width / 2) - (Screen.width / 2) * MenuFactorWidth,
								(Screen.height / 2) - (Screen.height / 2) * MenuFactorHeight,
								Screen.width * MenuFactorWidth, Screen.height * MenuFactorHeight);
	}

	void OnGUI() {
		if (MenuOverlayEnabled) {
			//windowRect = GUI.Window(0, windowRect, windowFunc, "Pause Menu");
		}
	}
	
	// Update is called once per frame
	void Update () {
	
	}*/
