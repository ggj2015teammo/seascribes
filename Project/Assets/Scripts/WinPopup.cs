﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class WinPopup : MonoBehaviour {

	private GUIStyle buttonStyle;
	public AudioSource krakenHitSound;

	private bool show = false;
	public void Show() {
		show = true;
	}

	protected void OnGUI() {
		if (show) {
			float hw = Screen.width / 2, hh = Screen.height / 2;
			Rect buttonRect = new Rect(hw - 200, hh - 250, 400, 400);

			//Set button style
			buttonStyle = new GUIStyle(GUI.skin.button);
			buttonStyle.fontSize = 24;
			krakenHitSound.Play ();

			if (GUI.Button(buttonRect, "You found the Kraken!\nYou won!\nThanks for playing\n\nClick to restart the game!", buttonStyle)) {
				Application.LoadLevel(1);
			}
		}
	}
}
