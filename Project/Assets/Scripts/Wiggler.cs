﻿using UnityEngine;
using System.Collections;

public class Wiggler : MonoBehaviour
{
	public Vector3 MinRotation;
	public Vector3 MaxRotation;
	public InitialState InitialRotation = InitialState.MiddleToMax;
	public float MinRotationSpeed = 1f;
	public float MaxRotationSpeed = 1f;

	public enum InitialState
	{
		Minimum,
		MiddleToMax,
		MiddleToMin,
		Maximum,
		Random
	}

	private float _progress;
	private Quaternion _minRot;
	private Quaternion _maxRot;
	private float _rotationSpeed;

	void Start()
	{
		_minRot = Quaternion.Euler(MinRotation);
		_maxRot = Quaternion.Euler(MaxRotation);

		_rotationSpeed = Random.Range(
			Mathf.Min(MinRotationSpeed, MaxRotationSpeed),
			Mathf.Max(MinRotationSpeed, MaxRotationSpeed));

		switch (InitialRotation)
		{
			case InitialState.Minimum:
				_progress = 0f;
				break;
			case InitialState.MiddleToMax:
				_progress = 0.25f;
				break;
			case InitialState.Maximum:
				_progress = 0.5f;
				break;
			case InitialState.MiddleToMin:
				_progress = 0.75f;
				break;
			case InitialState.Random:
				_progress = Random.Range(0f, 1f);
				break;
		}
	}

	void Update()
	{
		transform.localRotation = Quaternion.Lerp(_minRot, _maxRot, (Mathf.Sin(2f * Mathf.PI * _progress - Mathf.PI / 2f) + 1f) * 0.5f);
		_progress += Time.deltaTime * _rotationSpeed;
	}
}
