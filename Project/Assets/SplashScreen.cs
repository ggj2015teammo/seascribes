﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SplashScreen : MonoBehaviour {

	Timer t;

	protected void Start() {
		t = new Timer(3f);
	}

	protected void Update() {
		if (!t) {
			Application.LoadLevel(1);
		}
	}
}
